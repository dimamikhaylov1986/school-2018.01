from .server import ClientService

from argparse import ArgumentParser
import json
import Pyro4


def main():
    p = ArgumentParser(description="Proxy server")
    p.add_argument(
        "--config",
        help="Path to config.json file",
        default="config.json")
    args = p.parse_args()

    with open(args.config) as config_file:
        config = json.load(config_file)

    Pyro4.config.SERIALIZER = config["serializer"]
    Pyro4.config.SERIALIZERS_ACCEPTED.add(config["serializer"])

    daemon = Pyro4.Daemon(host=config["host"], port=config["port"])

    client_service = ClientService(config["master_uri"])
    daemon.register(client_service, "client")

    daemon.requestLoop()


if __name__ == "__main__":
    main()
